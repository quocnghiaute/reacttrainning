/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Start from './component/start';
import List from './component/list';
import Detail from './component/detail';

AppRegistry.registerComponent(appName, () => Detail);
