import React from "react";
import {StackNavigator} from "react-navigation";
import Start from './component/index';
import List from './component/list';

export const HomeStack = StackNavigator({
    StartScreen:{
        screen:Start
    },
    ListScreen:{
        screen:List
    },
})