var flatListData=[
    {
        "id":"1",
        "name":'Zen Meditation',
        "time":20,
        "linkImage":"require('../image/list1.png')"
    },
    {
        "id":"2",
        "name":'Replection',
        "time":6,
        "linkImage":require('../image/list2.png')
    },
    {
        "id":"3",
        "name":'Visualization',
        "time":13,
        "linkImage":require('../image/list3.png')
    },
    {
        "id":"4",
        "name":'Loving Kindness',
        "time":15,
        "linkImage":require('../image/list4.png')
    },
    {
        "id":"5",
        "name":'Focused Attention',
        "time":10,
        "linkImage":require('../image/list5.png')
    },
];
module.exports=flatListData;