import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Detail extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.appBar}>
          <Icon style={{ paddingLeft: 20 }} size={25} name='chevron-left'></Icon>
          <Icon style={{ paddingRight: 20 }} size={25} name='download'></Icon>
        </View>
        <View style={styles.bodyComponent}>
          <View style={styles.bodyTopComponent}>
            <View style={styles.boxImageComponent}>
              <View style={styles.boxImageChildComponent}>
                <Image source={require('../image/detail1.png')}></Image>
              </View>
            </View>
            <View style={{alignItems:'center'}}>
              <Text style={{fontSize: 22, fontWeight: 'bold' }}>Zen Meditation</Text>
              <Text style={{ marginTop: 5 }}>Inner Peace</Text>
            </View>

            <View style={{ flexDirection: 'row',alignItems:'center' }}>
              <Image source={require('../image/detail3.png')}></Image>
              <Image style={{marginHorizontal:25}} source={require('../image/detail4.png')}></Image>
              <Image source={require('../image/detail5.png')}></Image>
            </View>
            <View style={{ flexDirection: 'row', marginBottom: 50,alignItems:'center' }}>
              <Text>13:05</Text>
              <Image style={{marginHorizontal:25,marginTop:4}} source={require('../image/detail6.png')}></Image>
              <Text>15:00</Text>
            </View>
          </View>
          <View style={styles.bodyBottomComponent}>
            <Icon style={{ paddingLeft: 40 }} size={25} name='heart'></Icon>
            <Icon size={25} name='align-right'></Icon>
            <Icon style={{ paddingRight: 40 }} size={25} name='share-square'></Icon>
          </View>
        </View>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFACA"
  },
  appBar: {
    alignItems: "center",
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: "#FFFACA",
    flex: 1.1,
  },
  bodyComponent: {
    flex: 9,
    backgroundColor: 'white',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  bodyTopComponent: {
    justifyContent: 'space-between',
    flex: 8,
    alignItems: 'center'
  },
  bodyBottomComponent: {
    flex: 1.3,
    backgroundColor: "#FFE3D3",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    alignItems: "center",
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  boxImageComponent: {
    marginTop: 40,
    borderColor: "#FFE3D3",
    borderRadius: 110,
    borderWidth: 3,
    justifyContent: 'center',
    alignItems: 'center',
    height: 210,
    width: 210,
  },
  boxImageChildComponent:
  {
    backgroundColor: '#FFE3D3',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    height: 185,
    width: 185,
  }
});
