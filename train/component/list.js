import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import flatListData from '../data/data'

class FlatListItem extends Component {
  render() {
    if (this.props.index == 0) {
      return (
        <View style={{ borderRadius: 20, backgroundColor: "#FFE3D3",width:360}}>
          <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
            <Text style={{ fontSize: 21, fontWeight: 'bold', margin: 15 }}>{this.props.item.name}</Text>
            <View style={{ margin: 15, backgroundColor: "white", paddingHorizontal: 10, paddingVertical: 5, borderRadius: 20 }}>
              <Text>{this.props.item.time} min</Text>
            </View>
          </View>
          <View style={{ alignItems: 'center', paddingBottom: 20 }}>
            <Image source={require('../image/list1.png')}></Image>
          </View>

        </View>
      );
    } else {
      return (
        <View style={{marginTop:15,borderRadius: 20, backgroundColor: "#FFE3D3",width:172}}>
          <View style={{ flexDirection: 'column'}}>
            <Text style={{ fontSize: 21, fontWeight: 'bold', margin: 12 }}>{this.props.item.name}</Text>
            <View style={{marginLeft:15,paddingHorizontal:8,paddingVertical:3,backgroundColor: "white",borderRadius: 20,alignSelf:'baseline' }}>
              <Text>{this.props.item.time} min</Text>
            </View>
          </View>
          <View style={{ alignItems: 'center', paddingBottom: 20 }}>
            <Image source={this.props.item.linkImage}></Image>
          </View>

        </View>
      );
    }

  }
}

export default class List extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.appBar}>
          <View style={styles.logoComponent}>
            <View style={styles.iconComponent}>
              <Image source={require('../image/home3.png')}></Image>
            </View>
            <Text style={{ paddingTop: 5, paddingLeft: 5, fontSize: 16, fontWeight: "bold" }}>Hi,Martha</Text>
          </View>
          <Icon name="bars" size={20}></Icon>
        </View>
        <View style={styles.tabConponent}>
          <View>
            <Text>Sleep</Text>
          </View>
          <View style={styles.tabActiveConponent}>
            <Text style={styles.textActive}>Inner Peace</Text>
          </View>
          <View>
            <Text>Stress</Text>
          </View>
          <View>
            <Text>Anxiety</Text>
          </View>
        </View>
        <View style={styles.bodyConponent}>
          <FlatList 
          contentContainerStyle={styles.list}
          data={flatListData} renderItem={({ item, index }) => {
            return (<FlatListItem item={item} index={index}></FlatListItem>)
          }}>
          </FlatList>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 15,
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  appBar: {
    marginTop: 20,
    flexDirection: "row",
    height: 45,
    alignSelf: 'stretch',
    justifyContent: 'flex-end',
  },
  logoComponent: {
    flex: 1,
    flexDirection: "row"
  },
  iconComponent: {
    height: 33,
    justifyContent: 'flex-start',
    backgroundColor: "#FFE3D3",
    borderRadius: 5,
    padding: 5
  },
  tabConponent: {
    flexDirection: "row",
    marginTop: 10,
    height: 50,
    alignSelf: 'stretch',
    alignItems: "center",
    justifyContent: "space-between",
  },
  tabActiveConponent: {
    backgroundColor: "#393939",
    borderRadius: 30,
  },
  textActive: {
    color: "white",
    padding: 8
  },
  bannerComponent: {
    marginTop: 10,
    alignSelf: 'stretch',
    backgroundColor: "#FFE3D3",
    borderRadius: 20,
  },
  bodyConponent: {
    flex: 1,
    marginTop: 10,
    alignSelf: 'stretch',
  },
  list: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',
  }
});
