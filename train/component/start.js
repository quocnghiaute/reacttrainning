import React from 'react';
import { ImageBackground,StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';
import Icon  from 'react-native-vector-icons/FontAwesome5';

export default function Start() {
  return (
    <ImageBackground source={require('../image/home2.png')} style={styles.container}>
      <View style={styles.textComponet}>
            <View style={styles.subtileComponent}>
                <Icon size={15} style={{paddingTop:4}} name="angry"/>
                <Text style={styles.textIcon}>CalmMind</Text>
            </View>
            <Text style={styles.textTitle}>Stress Less and{"\n"}Laugh Louder</Text>
      </View>
      <View style={styles.imageComponent}>
          <Image source={require('../image/home1.png')}></Image>
      </View>
      <View style={styles.iconNextComponent}>
            <TouchableOpacity  onPress={() => { this.props.navigation.navigate("List")}}>
                 <Icon size={50} name="chevron-circle-right"></Icon>
            </TouchableOpacity>          
      </View>
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  textComponet: {
    flex: 2,
    paddingLeft: 25,
    paddingRight: 25,
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  imageComponent: {
    alignSelf: 'stretch',
    flex: 3,
  },
  iconNextComponent:{
    paddingBottom:20,
    justifyContent: 'flex-end',
    alignItems:"center",
    alignSelf: 'stretch',
    flex:1,
  },
  subtileComponent:{
        flexDirection:"row"
  },
  textIcon:{
    fontSize:18,
    color:"#ACB8C2",
    paddingLeft:5,
  },
  textTitle:{
    color:"black",
    fontSize:32,
    fontWeight:"bold"
  },
});
